from django.db import models

class Registration(models.Model):
    glugs = (
            ("AITCM","AIT-ChickMagluru"),
            ("GLUE","GLUE GEC Hassan"),
            ("GEON","GEON GEC Hassan"),
            ("Hassan","Hassan"),
            ("RIT","RIT Hassan"),
            ("BIT","BIT Mangalore"),
            ("PESCE-Mandya","PESCE Mandya"),
            ("Oxford","Oxford Bangalore"),
            ("SJBIT","SJBIT Bangalore"),
            ("SVIT","SVIT Bangalore"),
            ("VCET","VCET Mangalore"),
            ("FSMK","FSMK"),
            ("FSMK Community Center","FSMK Community Center")
            )
    participant_type = (
                ("participant","Participant"),
                ("volunteer","Volunteer"),
                ("speaker","Speaker")
            )
    payment_status = (
                ("To Pay","To be Paid"),
                ("Paid to co-ordinator","Paid to Co-ordinator"),
                ("Individual","Directly transferred to bank"),
                ("Camp","Will pay in camp"),
                ("unknown","Status Unknown. Call Participant")
            )

    gender = (
            ("male","Male"),
            ("female","Female"),
            ("others","Others")
            )
    arriving_status = (
                ("WA","Wednesday Afternoon ( 1 - 3 PM)"),
                ("WE","Wednesday Evening (3 - 5 PM)"),
                ("TM","Thursday Morning (4 -6:15AM)"),
                ("FM","Friday Morning"),
                ("SA","Saturday")
            )
    name = models.CharField(max_length=200)
    contact = models.PositiveIntegerField()
    email = models.EmailField()
    glug = models.CharField(max_length=100,choices=glugs)
    gender = models.CharField(max_length=30,choices=gender)
    semester = models.PositiveIntegerField()
    responsibility = models.TextField(blank=True, null=True)
    participant_type = models.CharField(max_length=100,choices=participant_type)
    payment_status = models.CharField(max_length=100,choices=payment_status)
    comments = models.TextField(blank=True,null=True)
    arriving_status = models.CharField(max_length=50,choices = arriving_status)

