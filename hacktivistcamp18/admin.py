from django.contrib import admin
from .models import Registration 
from django_admin_listfilter_dropdown.filters import DropdownFilter, ChoiceDropdownFilter

class RegistrationAdmin(admin.ModelAdmin):
    list_display = ["name","glug","gender","arriving_status"]
    search_fields = ["name","contact"]
    list_filter = (
        ('glug', ChoiceDropdownFilter),
        ('gender', ChoiceDropdownFilter),
        ('payment_status', ChoiceDropdownFilter),
        ('arriving_status', ChoiceDropdownFilter),
    )

admin.site.register(Registration,RegistrationAdmin)
