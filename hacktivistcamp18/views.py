from django.shortcuts import render
from .models import Registration

def find_participant(request):
    if request.method == "POST":
        contact = request.POST.get("mobile_number")
        if contact:
            participant = Registration.objects.filter(contact=contact).first()
            if participant:
                return render(request,"find_participant.html",{"participant":participant})
            else:
                return render(request,"find_participant.html",{"error":"Participant not found"})

    return render(request,"find_participant.html",{})
